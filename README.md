## Vita Kohana
- Kohana "port" for the PS Vita.
- Requires a desktop copy of the game (https://mushroomallow.itch.io/kohana).
- Currently a little stuttery and rough around the edges.

### Warnings
- Loading the game will take a bit of time.
- You should be able to access all buttons with the D-Pad (but if you can't, use the touchscreen).
- Animations may be somewhat stuttery on first load.

### Instructions
0. Download the VPK from the releases section of this repo.
1. Install the VPK on your Vita.
2. Locate the installation folder of the game on your computer.
3. Copy everything except `scripts.rpa` in the `game` folder to `ux0:app/RLYX00003/game/` on your Vita.

### Credits
- Made possible by SonicMastr's [Ren'Py Vita Project](https://github.com/SonicMastr/renpy-vita).
